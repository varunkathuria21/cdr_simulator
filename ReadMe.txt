To simulate the behaviour just run the test file by below command:

python test.py http://10.27.31.225 2 3 10 --wait=15 --repeat=5


There are total 6 command line arguments:
1) cdr_receiver_url
2) no of conference
3) no of participants
4) duration of call
5) wait --> It is sleep in between calls
6) repeat --> no of times we need to run the scenario

First 4 arguments are mandatory. Last 2 are optional
