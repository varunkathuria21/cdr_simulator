from cdrSimulate.calltype import call
from time import sleep
import argparse
import sys

arguments = sys.argv[1:]
if len(arguments) < 4:
    print "Less no of arguments"
    exit(0)

cdr_receiver_ip = sys.argv[1]
no_of_conf = (int)(sys.argv[2])
no_of_participants = (int)(sys.argv[3])
duration_of_calls = (int)(sys.argv[4])
repeat_val = 1
sleep_val = 0

for arg in sys.argv:
    if '--repeat' in arg:
        _, repeat_val = arg.split("=")
    elif '--wait' in arg:
        _, sleep_val = arg.split("=")

obj = call()
obj.set_cdrreciever(cdr_receiver_ip)
for i in range(int(repeat_val)):
    obj.create_call(no_of_conf,no_of_participants,duration_of_calls)
    if i < int(repeat_val) - 1:
        print "Waiting for %s seconds" %sleep_val
        sleep(int(sleep_val))




