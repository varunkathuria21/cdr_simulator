import xml.etree.ElementTree as etree


class CDRRecords(object):

    def __init__(self):
        self.correlatorIndexCount = 0

    def callStart(self, call_id, cospace):
        self.correlatorIndexCount += 1
        tree = etree.parse("xml/callstart.xml")
        root = tree.getroot()
        root.set('correlatorIndex', str(self.correlatorIndexCount))
        self.modifyxmlattribute(root, 'call', 'id', call_id)
        call = root.find('call')
        self.modifyxmlsubelement(call, 'coSpace', cospace)
        data = etree.tostring(root)
        return data

    def callEnd(self, call_id):
        self.correlatorIndexCount += 1
        tree = etree.parse("xml/callend.xml")
        root = tree.getroot()
        root.set('correlatorIndex', str(self.correlatorIndexCount))
        self.modifyxmlattribute(root, 'call', 'id', call_id)
        data = etree.tostring(root)
        return data

    def callLegStart(self, call_leg_id):
        self.correlatorIndexCount += 1
        tree = etree.parse("xml/calllegstart.xml")
        root = tree.getroot()
        root.set('correlatorIndex', str(self.correlatorIndexCount))
        self.modifyxmlattribute(root, 'callLeg', 'id', call_leg_id)
        data = etree.tostring(root)
        return data

    def callLegEnd(self, call_leg_id):
        self.correlatorIndexCount += 1
        tree = etree.parse("xml/calllegend.xml")
        root = tree.getroot()
        root.set('correlatorIndex', str(self.correlatorIndexCount))
        self.modifyxmlattribute(root, 'callLeg', 'id', call_leg_id)
        data = etree.tostring(root)
        return data

    def callLegUpdate(self, call_leg_id):
        self.correlatorIndexCount += 1
        tree = etree.parse("xml/calllegupdate.xml")
        root = tree.getroot()
        root.set('correlatorIndex', str(self.correlatorIndexCount))
        self.modifyxmlattribute(root, 'callLeg', 'id', call_leg_id)
        data = etree.tostring(root)
        return data

    def modifyxmlattribute(self, root, parent_element, attribute, value):
        element = root.find(parent_element)
        element.set(attribute, value)

    def modifyxmlsubelement(self, root, parent_element, value):
        element = root.find(parent_element)
        element.text = value
