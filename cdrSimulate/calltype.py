#!/usr/bin/python
# -*- coding: utf-8 -*-

import xml.etree.ElementTree as ET
from CDRRecords import CDRRecords
from timer import RepeatedTimer
import requests
import random
import string
import threading
from time import sleep

SESSION_ID = "a865433a-4926-4549-a701-9bb5b93c75e6"
BRIDGE_ID = "158ba4f7-70eb-4a35-982c-71d4f1674277"

class call(object):
    call_records = ET.Element('records')
    call_records.attrib['session'] = SESSION_ID
    call_records.attrib['callBridge'] = BRIDGE_ID

    def __init__(self):
        self.num = None
        self.rt = RepeatedTimer(2, self.post_data)
        self.cdr_record = CDRRecords()

    def start_call(self, call_id, call_leg_id, cospace, run_timer=False):
        self.call_records.append((ET.fromstring(self.cdr_record.callStart(call_id, cospace))))
        self.call_records.append((ET.fromstring(self.cdr_record.callLegStart(call_leg_id))))
        self.call_records.append((ET.fromstring(self.cdr_record.callLegUpdate(call_leg_id))))
        if not run_timer:
            pass
        elif self.rt.is_running:
            pass
        else:
            self.rt.start()

    def end_call(self, *args, **kwargs):
        call_id = kwargs['call_id']
        call_leg_id = kwargs['call_leg_id']
        run_timer = kwargs['run_timer']
        self.call_records.append((ET.fromstring(self.cdr_record.callLegEnd(call_leg_id))))
        self.call_records.append((ET.fromstring(self.cdr_record.callEnd(call_id))))
        print ("Ending call with call id::{}, call_leg_id::{}".format(call_id, call_leg_id))
        if not run_timer:
            pass
        elif self.rt.is_running:
            pass
        else:
            self.rt.start()

    def create_call(self, no_of_conference, no_of_participants, duration):
        run_timer = False
        for x in xrange(no_of_conference):
            call_id = self.id_generator()
            cospace = self.id_generator()
            for y in xrange(no_of_participants):
                call_leg_id = self.id_generator()
                if y == no_of_participants-1 and x == no_of_conference-1:
                    run_timer = True
                self.start_call(call_id, call_leg_id, cospace, run_timer)
                print ("Call created with call id::{}, call_leg_id::{} & cospace::{}".format(call_id, call_leg_id, cospace))
                t = threading.Timer(duration, self.end_call, (), {'call_id': call_id, 'call_leg_id': call_leg_id, 'run_timer': run_timer})
                t.start()

    def post_data(self):
        XML = ET.tostring(self.call_records)
        try:
            response = requests.post(self.cdr_rcvr, data=XML)
            if response.status_code == 200:
                self.rt.stop()
                ''' Resetting Call Records'''
                self.call_records = ET.Element('records')
                self.call_records.attrib['session'] = SESSION_ID
                self.call_records.attrib['callBridge'] = BRIDGE_ID
            else:
                print (response.status_code, response.reason)
        except requests.ConnectionError:
            print "Connection Error"
        except requests.HTTPError:
            print "HTTP Error"

    def id_generator(self):
        char_set = string.ascii_lowercase + string.digits
        id1 = ''.join(random.sample(char_set*6, 8))
        id2 = ''.join(random.sample(char_set*6, 4))
        id3 = ''.join(random.sample(char_set*6, 4))
        id4 = ''.join(random.sample(char_set*6, 4))
        id5 = ''.join(random.sample(char_set*6, 12))
        id = id1+'-'+id2+'-'+id3+'-'+id4+'-'+id5
        return id

    def set_cdrreciever(self, cdr_receiver="bugs.python.org"):
        self.cdr_rcvr = cdr_receiver
